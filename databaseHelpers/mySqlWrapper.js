module.exports = {

  query: query
}

//get the mySql object
var pg = require('pg');

//object which holds the connection to the db
//let connection = null

var connectionString= 'postgres://pguser:pguser@ec2-52-210-105-124.eu-west-1.compute.amazonaws.com:5432/pgdb';

/**
 * Create the connection to the db
 */


// function initConnection() {
//
//   pg.connect(connectionString, onConnect);
//
// function onConnect(err, client, done) {
//   //Err - This means something went wrong connecting to the database.
//   if (err) {
//     console.error(err);
//     process.exit(1);
//   }
//
//   //For now let's end client
//   client.end();
// }
//
// }

/**
 * executes the specified sql query and provides a callback which is given
 * with the results in a DataResponseObject
 *
 * @param queryString
 * @param callback - takes a DataResponseObject
 */
function query(queryString, callback){

  //init the connection object. Needs to be done everytime as we call end()
  //on the connection after the call is complete
  //initConnection()
  var clientePostgre = new pg.Client(connectionString);
  //connect to the db
  console.log("conect 1 : "+callback);
  clientePostgre.connect();

      //execute the query and collect the results in the callback
      clientePostgre.query(queryString, function(error, results, fields){

          // console.log('mySql: query: error is: ', error, ' and results are: ', results);

        //disconnect from the method
        clientePostgre.end();

        //send the response in the callback
        callback(createDataResponseObject(error, results))
      })

}

/**
 * creates and returns a DataResponseObject made out of the specified parameters.
 * A DataResponseObject has two variables. An error which is a boolean and the results of the query.
 *
 * @param error
 * @param results
 * @return {DataResponseObject<{error, results}>}
 */
function createDataResponseObject(error, results) {
    console.log('create response '+results);
    return {
      error: error,
      results: results === undefined ? null : results === null ? null : results
     }
  }
